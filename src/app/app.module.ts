import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
// import {AuthorizationInterceptor} from './authorization/authorization.interceptor';
import {AuthorizationService} from './authorization/authorization.service';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AppRoutingModule} from './app.routing.module';
import {Services} from './services/services';
import {CoreModule} from './core/core.module';
import { GuidesComponent } from './guides/guides.component';
import { ClanwarsComponent } from './clanwars/clanwars.component';
import {ClanNewsComponent} from './clan-info/clan-news.component';

@NgModule({
  declarations: [
    AppComponent,
    GuidesComponent,
    ClanwarsComponent,
    ClanNewsComponent,
  ],
  imports: [
    BrowserModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    CoreModule
  ],
  providers: [AuthorizationService, Services,
    // {provide: HTTP_INTERCEPTORS, useClass: AuthorizationInterceptor, multi: true}
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
