import {NgModule} from '@angular/core';
import {NavbarComponent} from './navbar/navbar.component';
import {AppRoutingModule} from '../app.routing.module';
import {Services} from '../services/services';
import {HomeComponent} from './home/home.component';
import {CommonModule} from '@angular/common';

@NgModule({
  declarations: [
    NavbarComponent,
    HomeComponent,
  ],
  imports: [
    AppRoutingModule,
    CommonModule
  ],
  exports: [
    AppRoutingModule,
    NavbarComponent,
    HomeComponent
  ],
  providers: [
    Services
  ]
})
export class CoreModule {

}
