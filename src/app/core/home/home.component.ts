import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Services} from '../../services/services';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  data: any;
  clanScore: any;
  clanName: any;
  clanDescription: any;
  clanWarTrophies: any;
  clanMembers: any;
  arenaId: number;
  arenaString: string;

  constructor(private http: HttpClient, private service: Services) {
  }

  ngOnInit() {
    window.scroll(0, 0);
    this.getMyClan();
    this.getClanMembers();
  }

  getCards() {
    this.service.getCards().subscribe(data => {
      this.data = data;
      console.log(this.data.items);
    });
  }

  getMyClan() {
    this.service.getMyClan('%23RR9UV9L').subscribe(data => {
      this.data = data;
      this.clanScore = this.data.clanScore;
      this.clanName = this.data.name;
      this.clanDescription = this.data.description;
      this.clanWarTrophies = this.data.clanWarTrophies;
    });
  }

  getClanMembers() {
    this.service.getClanMembers('%23RR9UV9L').subscribe(data => {
      const response = data;
      this.clanMembers = response.items;
    });
  }

  search(nameKey: string, myArray) {
    for (let i = 0; i < myArray.length; i++) {
      if (myArray[i].name === nameKey) {
        return myArray[i];
      }
    }
  }
}
