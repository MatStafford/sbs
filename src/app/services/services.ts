import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';


@Injectable()
export class Services {

  constructor(private http: HttpClient) {
  }

  // Card Call
  public getCards(): Observable<object> {
    return this.http.get<object>('/services/getCards').pipe(map(res => res));
  }

  // Clan Calls
  public getAllClans(): Observable<object> {
    return this.http.get<object>('/services/clans');
  }

  public getMyClan(clanTag: string): Observable<object> {
    return this.http.get<object>('/services/clans/' + clanTag);
  }

  public getClanMembers(clanTag: string): Observable<any> {
    return this.http.get('/services/clans/' + clanTag + '/members');
  }

  public getClanWarlog(clanTag: string): Observable<object> {
    return this.http.get<object>('/services/clans/:clanTag/warlog');
  }

  public getClanCurrentWar(clanTag: string): Observable<object> {
    return this.http.get<object>('/services/clans/:clanTag/currentwar');
  }

  // Player Calls
  public getPlayer(playerTag: string): Observable<object> {
    return this.http.get<object>('/services/players/:playerTag');
  }

  public getPlayerUpcomingChest(playerTag: string): Observable<object> {
    return this.http.get<object>('/services/players/:playerTag/upcomingchests');
  }

  public getPlayerWarlog(playerTag: string): Observable<object> {
    return this.http.get<object>('/services/players/:playerTag/warlog');
  }

  // Tournament Calls
  public getTournaments(): Observable<object> {
    return this.http.get<object>('/services/tournaments');
  }

  public getSingleTournament(tournamentTag: string): Observable<object> {
    return this.http.get<object>('/services/tournaments/:tournamentTag');
  }

  public getGlobalTournaments(): Observable<object> {
    return this.http.get<object>('/services/globaltournaments');
  }

  // Location Calls
  public getLocations(): Observable<object> {
    return this.http.get<object>('/services/locations');
  }

  public getLocationId(locationId: string): Observable<object> {
    return this.http.get<object>('/services/locations/:locationId');
  }

  public getClanRankByLocation(locationId: string): Observable<object> {
    return this.http.get<object>('/services/locations/:locationId/rankings/clan');
  }

  public getPlayerRankByLocation(locationId: string): Observable<object> {
    return this.http.get<object>('/services/locations/:locationId/rankings/players');
  }

  public getClanwarsByLocation(locationId: string): Observable<object> {
    return this.http.get<object>('/services/locations/:locationId/rankings/clanwars');
  }
}
