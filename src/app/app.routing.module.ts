import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {AppComponent} from './app.component';
import {HomeComponent} from './core/home/home.component';
import {ClanNewsComponent} from './clan-info/clan-news.component';
import {ClanwarsComponent} from './clanwars/clanwars.component';
import {GuidesComponent} from './guides/guides.component';

const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: 'clan-news', component: ClanNewsComponent},
  {path: 'clan-wars', component: ClanwarsComponent},
  {path: 'guides', component: GuidesComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule {
}

export const routedComponents = [AppComponent];
