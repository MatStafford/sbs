import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClanwarsComponent } from './clanwars.component';

describe('ClanwarsComponent', () => {
  let component: ClanwarsComponent;
  let fixture: ComponentFixture<ClanwarsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClanwarsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClanwarsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
